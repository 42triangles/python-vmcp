#!/usr/bin/env python3

"""OSC example."""

from typing import Any
from vmcp.osc import (
    OSC,
    SENDER,
    RECEIVER
)
from vmcp.osc.typing import (
    Message,
    ARG_DATAUNPACK,
    ARG_READERNAME
)


LISTENING: bool = True


def received(*args: Any):
    """Receive transmission."""
    global LISTENING  # pylint: disable=global-statement
    print(args)
    LISTENING = False


with OSC() as osc:
    in1 = osc.receiver(
        osc.create_channel(
            "127.0.0.1",
            39539,
            "example3",
            RECEIVER
        )
    )
    in1.register_handler(
        "/VMC/Ext/Root/Pos",
        received,
        ARG_READERNAME + ARG_DATAUNPACK
    )
    out1 = osc.sender(
        osc.create_channel(
            "127.0.0.1",
            39539,
            "example1",
            SENDER
        )
    )
    out2 = osc.sender(
        osc.create_channel(
            "127.0.0.1",
            39540,
            "example2",
            SENDER
        )
    )
    out1.send(Message("/test/one", ",sif", ["first", 672, 8.871]))
    out2.send(
        (
            Message("/test/one", ",sif", ["second", 672, 8.871]),
            Message("/test/two", None, ["third", 234, 2.513])  # None=auto
        )
    )
    while LISTENING:
        in1.run()
