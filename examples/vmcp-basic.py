#!/usr/bin/env python3

"""VMCP Example."""

# Logging
from logging import (
    basicConfig as set_logger_config,
    getLogger as get_logger,
    DEBUG as LOGLEVEL_DEBUG
)
# For delayed processing example below
from time import time
# OSC network protocol
from vmcp.osc import (
    OSC,
    SENDER,
    RECEIVER
)
from vmcp.osc.typing import TimeTag  # For delayed processing example below
# VMC protocol layer
from vmcp import VMCP
from vmcp.events import (
    Event,
    RootTransformEvent,
    BoneTransformEvent,
    BlendShapeEvent,
    BlendShapeApplyEvent,
    DeviceTransformEvent
)
from vmcp.typing import (
    Bone,
    DeviceType,
    Scale,
    BlendShapeKey as AbstractBlendShapeKey
)
from vmcp.math import (
    CoordinateVector,
    Quaternion
)

FRM = "%(asctime)s - %(threadName)s ø %(name)s - %(levelname)s - %(message)s"
set_logger_config(
    filename="vmc.log",
    filemode='a',
    encoding="utf-8",
    format=FRM
)
logger = get_logger("vmc")
logger.setLevel(LOGLEVEL_DEBUG)


class BlendShapeKey(AbstractBlendShapeKey):
    """Example model blend shape keys.

    Depends on the used avatar model.

    """

    # Mouth movement while speaking
    VOCAL_A = "A"
    VOCAL_I = "I"
    VOCAL_U = "U"
    VOCAL_E = "E"
    VOCAL_O = "O"
    # Eye blink
    EYES_BLINK_BOTH = "Blink"
    EYES_BLINK_L = "Blink_L"
    EYES_BLINK_R = "Blink_R"
    # Face expressions
    FACE_NEUTRAL = "Neutral"
    FACE_ANGRY = "Angry"
    FACE_FUN = "Fun"
    FACE_JOY = "Joy"
    FACE_SORROW = "Sorrow"
    FACE_SURPRISED = "Surprised"
    FACE_LOOK_UP = "LookUp"
    FACE_LOOK_DOWN = "LookDown"
    FACE_LOOK_LEFT = "LookLeft"
    FACE_LOOK_RIGHT = "LookRight"


LISTENING = True


def received(event: Event):
    """Receive transmission."""
    print(repr(event))
    if isinstance(event, DeviceTransformEvent):
        global LISTENING  # pylint: disable=global-statement
        print(event.device_type)
        print(event.is_local)
        LISTENING = False


with OSC(logger=logger) as osc:
    in1 = osc.receiver(
        osc.create_channel(
            "127.0.0.1",
            39539,
            "example1",
            RECEIVER
        )
    )
    in1.register_handler(*VMCP.on_receive(RootTransformEvent, received))
    in1.register_handler(*VMCP.on_receive(BoneTransformEvent, received))
    in1.register_handler(*VMCP.on_receive(BlendShapeEvent, received))
    in1.register_handler(*VMCP.on_receive(BlendShapeApplyEvent, received))
    in1.register_handler(*VMCP.on_receive(DeviceTransformEvent, received))
    with osc.sender(
        osc.create_channel(
            "127.0.0.1",
            39539,
            "example2",
            SENDER
        )
    ) as out1:
        out1.send(
            VMCP.root_transform(
                CoordinateVector(1.0, 1.0, 1.0),
                Quaternion.identity(),
                Scale(2.5),
                CoordinateVector(1, 2, 3)
            ),
            TimeTag.from_unixtime(time() + 5)  # 5 seconds processing delay
        )
        out1.send(
            (
                VMCP.bone_transform(
                    Bone.LEFT_UPPER_LEG,
                    CoordinateVector.identity(),
                    Quaternion.from_euler('xyz', [0, 0, -45], degrees=True)
                ),
                VMCP.bone_transform(
                    Bone.RIGHT_LOWER_ARM,
                    CoordinateVector.identity(),
                    Quaternion(0.0, 0.0, 0.0, 1.0)
                ),
                VMCP.blendshape(
                    BlendShapeKey.FACE_FUN,
                    1.0
                ),
                VMCP.blendshape(
                    BlendShapeKey.EYES_BLINK_R,
                    1.0
                ),
                VMCP.blendshape_apply()
            )
        )
        out1.send(
            VMCP.device_transform(
                DeviceType.HMD,
                "Head",
                CoordinateVector.identity(),
                Quaternion.identity(),
                True
            ),
            TimeTag.from_unixtime(time() + 5)  # 5 seconds processing delay
        )
    while LISTENING:
        in1.run()
