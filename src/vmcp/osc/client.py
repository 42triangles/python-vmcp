#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# SPDX-License-Identifier: AGPL-3.0-or-later

"""``client`` module of ``osc`` package."""

from typing import (
    Union,
    Tuple,
    List,
    Literal
)
from types import TracebackType
from osc4py3.as_eventloop import (
    osc_udp_client,
    osc_send,
    osc_process
)
from .typing import (
    Message,
    MessageBundle,
    TimeTag,
    IMMEDIATELY
)


class Client:
    """OSC client."""

    @property
    def host(self) -> str:
        """str: Host DNS or IP address."""
        return self._host

    @property
    def port(self) -> int:
        """int: Host port."""
        return self._port

    @property
    def name(self) -> str:
        """str: Client's name."""
        return self._name

    def __init__(self, host: str, port: int, name: str) -> None:
        """OSC client constructor.

        Args:
            host (str):
                Client's host DNS or IP (recommended).
            port (int):
                Port.
            name (str):
                Client's name (arbitrary, but need to be unique).

        """
        self._host = host
        self._port = port
        self._name = name
        osc_udp_client(
            self._host,
            self._port,
            self._name
        )

    def __enter__(self) -> 'Client':
        """Return the object instance for usage of the ```with`` statement.

        Returns:
            Client:
                Object instance.

        """
        return self

    def send(
        self,
        data: Union[Message, Tuple[Message, ...], List[Message]],
        delay: TimeTag = IMMEDIATELY
    ) -> None:
        """Send OSC message.

        Args:
            data (Union[Message, Tuple[Message, ...], List[Message]]):
                One or many ``Message``(s).
            delay (Optional[TimeTag]):
                Message processing delay (Default: IMMEDIATELY)

        """
        packet: tuple[Message, ...] = ()
        if isinstance(data, Message):
            packet = (data,)
        else:
            packet = tuple(data)
        osc_send(
            MessageBundle(
                delay,
                packet
            ),
            self._name
        )
        osc_process()

    def __eq__(self, other) -> bool:
        """Return ``True`` if an object is identical to ``other`` object.

        Args:
            other (Client):
                Other object for comparison.

        Returns:
            bool:
                ``True`` if identical, otherwise ``False``.

        """
        return (
            self.host == other.host and
            self.port == other.port and
            self.name == other.name
        )

    def __hash__(self) -> int:
        """Return hash value to quickly compare keys for dictionary lookup.

        Returns:
            int:
                Hash value.

        """
        return hash(
            (
                self.host,
                self.port,
                self.name
            )
        )

    def __exit__(
        self,
        exc_type: type[BaseException] = None,
        exc_value: BaseException = None,
        traceback: TracebackType = None
    ) -> Literal[False]:
        """Return everytime ``False`` if exiting the runtime context.

        Args:
            exc_type (Optional[type[BaseException]]):
                Exception type.
            exc_value (Optional[BaseException]):
                Exception instance.
            traceback (Optional[TracebackType]):
                Exception context.

        Returns:
            bool:
                Everytime ``False``.

        """
        return False
