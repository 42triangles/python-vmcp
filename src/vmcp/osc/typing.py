#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# SPDX-License-Identifier: AGPL-3.0-or-later

"""``typing`` module of ``osc`` package."""

from typing import (
    Protocol,
    runtime_checkable,
    Callable,
    Union,
    Tuple,
    List,
    Any
)
from types import TracebackType
from abc import ABCMeta, abstractmethod
from osc4py3.oscbuildparse import (
    OSCMessage,
    OSCBundle,
    OSCtimetag,
    OSC_IMMEDIATELY,
    unixtime2timetag
)
from osc4py3.oscmethod import (
    OSCARG_DATA,
    OSCARG_DATAUNPACK,
    OSCARG_MESSAGE,
    OSCARG_MESSAGEUNPACK,
    OSCARG_ADDRESS,
    OSCARG_TYPETAGS,
    OSCARG_EXTRA,
    OSCARG_EXTRAUNPACK,
    OSCARG_METHODFILTER,
    OSCARG_PACKOPT,
    OSCARG_READERNAME,
    OSCARG_SRCIDENT,
    OSCARG_READTIME
)

IMMEDIATELY = OSC_IMMEDIATELY

ARG_DATA = OSCARG_DATA
ARG_DATAUNPACK = OSCARG_DATAUNPACK
ARG_MESSAGE = OSCARG_MESSAGE
ARG_MESSAGEUNPACK = OSCARG_MESSAGEUNPACK
ARG_ADDRESS = OSCARG_ADDRESS
ARG_TYPETAGS = OSCARG_TYPETAGS
ARG_EXTRA = OSCARG_EXTRA
ARG_EXTRAUNPACK = OSCARG_EXTRAUNPACK
ARG_METHODFILTER = OSCARG_METHODFILTER
ARG_PACKOPT = OSCARG_PACKOPT
ARG_READERNAME = OSCARG_READERNAME
ARG_SRCIDENT = OSCARG_SRCIDENT
ARG_READTIME = OSCARG_READTIME


class Message(OSCMessage):
    """OSC message interface."""


class MessageBundle(OSCBundle):
    """OSC message bundle interface."""


class TimeTag(OSCtimetag):
    """OSC time tag interface."""

    @classmethod
    def from_unixtime(cls, time: float) -> 'TimeTag':
        """Create ``TimeTag`` from UNIX epoch time.

        Args:
            time (float): UNIX epoch time (seconds since 1/1/1970).

        Returns:
            TimeTag: Seconds since 1/1/1900 (based on NTP timestamps).

        """
        timetag = unixtime2timetag(time)
        return cls(timetag.sec, timetag.frac)


@runtime_checkable
class Sender(Protocol, metaclass=ABCMeta):
    """OSC sender interface."""

    @property
    @abstractmethod
    def host(self) -> str:
        """str: Host DNS or IP address."""
        raise NotImplementedError

    @property
    @abstractmethod
    def port(self) -> int:
        """int: Host port."""
        raise NotImplementedError

    @property
    @abstractmethod
    def name(self) -> str:
        """str: Sender's name."""
        raise NotImplementedError

    def __init__(self, host: str, port: int, name: str) -> None:
        """OSC sender constructor.

        Args:
            host (str):
                Sender's host DNS or IP (recommended).
            port (int):
                Port.
            name (str):
                Sender's name (arbitrary, but need to be unique).

        """
        raise NotImplementedError

    def __enter__(self) -> 'Sender':
        """Return the object instance for usage of the ```with`` statement.

        Returns:
            Sender:
                Object instance.

        """
        raise NotImplementedError

    def send(
        self,
        data: Union[Message, Tuple[Message, ...], List[Message]],
        delay: TimeTag = IMMEDIATELY
    ) -> None:
        """Send OSC message.

        Args:
            data (Union[Message, Tuple[Message, ...], List[Message]]):
                One or many ``Message``(s).
            delay (Optional[TimeTag]):
                Message processing delay (Default: IMMEDIATELY)

        """
        raise NotImplementedError

    def __eq__(self, other) -> bool:
        """Return ``True`` if an object is identical to ``other`` object.

        Args:
            other (Sender):
                Other object for comparison.

        Returns:
            bool:
                ``True`` if identical, otherwise ``False``.

        """
        raise NotImplementedError

    def __hash__(self) -> int:
        """Return hash value to quickly compare keys for dictionary lookup.

        Returns:
            int:
                Hash value.

        """
        raise NotImplementedError

    def __exit__(
        self,
        exc_type: type[BaseException] = None,
        exc_value: BaseException = None,
        traceback: TracebackType = None
    ) -> bool:
        """Return everytime ``False`` if exiting the runtime context.

        Args:
            exc_type (Optional[type[BaseException]]):
                Exception type.
            exc_value (Optional[BaseException]):
                Exception instance.
            traceback (Optional[TracebackType]):
                Exception context.

        Returns:
            bool:
                Everytime ``False``.

        """
        raise NotImplementedError


@runtime_checkable
class Receiver(Protocol, metaclass=ABCMeta):
    """OSC receiver interface."""

    @property
    @abstractmethod
    def host(self) -> str:
        """str: Host DNS or IP address."""
        raise NotImplementedError

    @property
    @abstractmethod
    def port(self) -> int:
        """int: Host port."""
        raise NotImplementedError

    @property
    @abstractmethod
    def name(self) -> str:
        """str: Receiver's name."""
        raise NotImplementedError

    def __init__(self, host: str, port: int, name: str) -> None:
        """OSC receiver constructor.

        Args:
            host (str):
                Listening DNS or IP (recommended).
            port (int):
                Listening port.
            name (str):
                Receiver's name (arbitrary, but need to be unique).

        """
        raise NotImplementedError

    def __enter__(self) -> 'Receiver':
        """Return the object instance for usage of the ```with`` statement.

        Returns:
            Receiver:
                Object instance.

        """
        raise NotImplementedError

    def register_handler(
        self,
        address: str,
        handler: Callable[..., None],
        scheme: Tuple[str] = ARG_DATAUNPACK,
        extra: Any = None
    ) -> None:
        """Register receiving handler.

        Args:
            address (str):
                OSC address pattern filter string expression.
            handler (Callable[..., None]):
                Handler function.
            scheme (Optional[Tuple[str]]):
                Argument schemes (Default: ARG_DATAUNPACK).
            extra (Optional[Any]):
                Extra parameter(s) for your handler function.

        """
        raise NotImplementedError

    def run(self) -> None:
        """Process on every invoke."""
        raise NotImplementedError

    def __eq__(self, other) -> bool:
        """Return ``True`` if an object is identical to ``other`` object.

        Args:
            other (Client):
                Other object for comparison.

        Returns:
            bool:
                ``True`` if identical, otherwise ``False``.

        """
        raise NotImplementedError

    def __hash__(self) -> int:
        """Return hash value to quickly compare keys for dictionary lookup.

        Returns:
            int:
                Hash value.

        """
        raise NotImplementedError

    def __exit__(
        self,
        exc_type: type[BaseException] = None,
        exc_value: BaseException = None,
        traceback: TracebackType = None
    ) -> bool:
        """Return everytime ``False`` if exiting the runtime context.

        Args:
            exc_type (Optional[type[BaseException]]):
                Exception type.
            exc_value (Optional[BaseException]):
                Exception instance.
            traceback (Optional[TracebackType]):
                Exception context.

        Returns:
            bool:
                Everytime ``False``.

        """
        raise NotImplementedError
