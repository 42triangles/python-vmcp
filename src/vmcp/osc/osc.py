#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# SPDX-License-Identifier: AGPL-3.0-or-later

"""System module of ``osc`` package."""

from logging import Logger
from typing import Union, Type, Literal
from types import TracebackType
from osc4py3.as_eventloop import osc_startup, osc_terminate
from .typing import Sender, Receiver


class OSC:
    """Open Sound Control (OSC) network protocol system."""

    def __init__(
        self,
        logger: Logger = None
    ) -> None:
        """OSC constructor.

        Args:
            logger (Optional[Logger]):
                Logger instance.

        """
        osc_startup(
            logger=logger
        )

    def __enter__(self) -> 'OSC':
        """Return the object instance for usage of the ```with`` statement.

        Returns:
            OSC:
                Object instance.

        """
        return self

    def create_channel(
        self,
        host: str,
        port: int,
        channel_name: str,
        channel_type: Union[Type[Sender], Type[Receiver]]
    ) -> str:
        """Create OSC channel.

        Args:
            host (str):
                Host DNS or IP (recommended).
            port (int):
                Port.
            channel_name (str):
                Channel name (arbitrary, but need to be unique).
            channel_type (Union[Type[Sender], Type[Receiver]]):
                Channel type (``Sender`` or ``Receiver``).

        Returns:
            str:
                Channel name.

        Raises:
            ValueError:
                If ``channel_type`` is not ``Sender`` or ``Receiver``.

        """
        if isinstance(channel_type, Sender):
            attribute = "_sender"
        elif isinstance(channel_type, Receiver):
            attribute = "_receiver"
        else:
            raise ValueError("Wrong channel type given.")
        instances = getattr(self, attribute, dict())
        instances[channel_name] = channel_type(host, port, channel_name)
        setattr(self, attribute, instances)
        return channel_name

    def sender(self, channel_name: str) -> Sender:
        """Get sender channel instance.

        Args:
            channel_name (str):
                Channel name (unique).

        Returns:
            Sender:
                Channel instance.

        Raises:
            KeyError:
                If ``channel_name`` does not exists.

        """
        return getattr(self, "_sender", dict())[channel_name]

    def receiver(self, channel_name: str) -> Receiver:
        """Get receiver channel instance.

        Args:
            channel_name (str):
                Channel name (unique).

        Returns:
            Receiver:
                Channel instance.

        Raises:
            KeyError:
                If ``channel_name`` does not exists.

        """
        return getattr(self, "_receiver", dict())[channel_name]

    def __exit__(
        self,
        exc_type: type[BaseException] = None,
        exc_value: BaseException = None,
        traceback: TracebackType = None
    ) -> Literal[False]:
        """Return everytime ``False`` if exiting the runtime context.

        Args:
            exc_type (Optional[type[BaseException]]):
                Exception type.
            exc_value (Optional[BaseException]):
                Exception instance.
            traceback (Optional[TracebackType]):
                Exception context.

        Returns:
            bool:
                Everytime ``False``.

        """
        osc_terminate()
        return False
