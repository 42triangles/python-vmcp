#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# SPDX-License-Identifier: AGPL-3.0-or-later

"""``server`` module of ``osc`` package."""

from typing import Callable, Tuple, Any, Literal
from types import TracebackType
from osc4py3.as_eventloop import osc_udp_server, osc_method, osc_process
from osc4py3.oscmethod import OSCARG_DATAUNPACK


class Server:
    """OSC server."""

    @property
    def host(self) -> str:
        """str: Host DNS or IP address."""
        return self._host

    @property
    def port(self) -> int:
        """int: Host port."""
        return self._port

    @property
    def name(self) -> str:
        """str: Server's name."""
        return self._name

    def __init__(self, host: str, port: int, name: str) -> None:
        """OSC server constructor.

        Args:
            host (str):
                Listening DNS or IP (recommended).
            port (int):
                Listening port.
            name (str):
                Server's name (arbitrary, but need to be unique).

        """
        self._host = host
        self._port = port
        self._name = name
        osc_udp_server(
            self._host,
            self._port,
            self._name
        )

    def __enter__(self) -> 'Server':
        """Return the object instance for usage of the ```with`` statement.

        Returns:
            Server:
                Object instance.

        """
        return self

    def register_handler(
        self,
        address: str,
        handler: Callable[..., None],
        scheme: Tuple[str] = OSCARG_DATAUNPACK,
        extra: Any = None
    ) -> None:
        """Register receiving handler.

        Args:
            address (str):
                OSC address pattern filter string expression.
            handler (Callable[..., None]):
                Handler function.
            scheme (Optional[Tuple[str]]):
                Argument schemes (Default: ARG_DATAUNPACK).
            extra (Optional[Any]):
                Extra parameter(s) for your handler function.

        """
        osc_method(address, handler, scheme, extra)

    def run(self) -> None:
        """Process on every invoke."""
        osc_process()

    def __eq__(self, other) -> bool:
        """Return ``True`` if an object is identical to ``other`` object.

        Args:
            other (Client):
                Other object for comparison.

        Returns:
            bool:
                ``True`` if identical, otherwise ``False``.

        """
        return (
            self.host == other.host and
            self.port == other.port and
            self.name == other.name
        )

    def __hash__(self) -> int:
        """Return hash value to quickly compare keys for dictionary lookup.

        Returns:
            int:
                Hash value.

        """
        return hash(
            (
                self.host,
                self.port,
                self.name
            )
        )

    def __exit__(
        self,
        exc_type: type[BaseException] = None,
        exc_value: BaseException = None,
        traceback: TracebackType = None
    ) -> Literal[False]:
        """Return everytime ``False`` if exiting the runtime context.

        Args:
            exc_type (Optional[type[BaseException]]):
                Exception type.
            exc_value (Optional[BaseException]):
                Exception instance.
            traceback (Optional[TracebackType]):
                Exception context.

        Returns:
            bool:
                Everytime ``False``.

        """
        return False
