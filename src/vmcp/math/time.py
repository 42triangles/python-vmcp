#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# SPDX-License-Identifier: AGPL-3.0-or-later

"""```time`` module of ``math`` package."""

from time import time
from .typing import Reproducible


class Timestamp(float, Reproducible):
    """Timestamp since the epoch in seconds with it's fractions."""

    def __new__(cls) -> 'Timestamp':
        """Create a new instance of class ``Timestamp``.

        Returns:
            Timestamp:
                Object instance.

        """
        return super().__new__(cls, time())

    def delta(self, offset: float = 0.0, precision: int = 6) -> float:
        """Return time delta in seconds with it's fractions as float.

        Args:
            offset (Optional[float]):
                Delta offset (Default: 0.0).
            precision (Optional[int]):
                Round the results to ``precision`` digits
                after the decimal point (Default: 6).

        Returns:
            float:
                Time delta in seconds with it's fractions.

        >>> Timestamp().delta()
        0.0

        """
        return round(offset + time() - self, precision)
