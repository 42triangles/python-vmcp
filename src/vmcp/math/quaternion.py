#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# SPDX-License-Identifier: AGPL-3.0-or-later

"""```quaternion`` module of ``math`` package."""

from typing import overload, Union
from scipy.spatial.transform import Rotation as RotationLogic
from .typing import Reproducible


class Quaternion(RotationLogic, Reproducible):
    """Quaternion."""

    @property
    def x(self) -> float:  # pylint: disable=invalid-name
        """float: x component."""
        return float(self.as_quat()[0])

    @property
    def y(self) -> float:  # pylint: disable=invalid-name
        """float: y component."""
        return float(self.as_quat()[1])

    @property
    def z(self) -> float:  # pylint: disable=invalid-name
        """float: z component."""
        return float(self.as_quat()[2])

    @property
    def w(self) -> float:  # pylint: disable=invalid-name
        """float: w component."""
        return float(self.as_quat()[3])

    @overload
    def __init__(self, quat, normalize=True, copy=True) -> None:
        """Quaternion constructor.

        Args:
            quat (tuple):
                Each row is a (possibly non-unit norm) quaternion
                in scalar-last (x, y, z, w) format.
            normalize (Optional[bool]):
                If True, each quaternion will be normalized to unit norm.
            copy (Optional[bool]):
                Uses an copy of the given quat
                (If normalize=True, it's everytime True).

        """
        raise NotImplementedError

    @overload
    def __init__(
        self,  # pylint: disable=invalid-name
        x: Union[float, int],  # pylint: disable=invalid-name
        y: Union[float, int],  # pylint: disable=invalid-name
        z: Union[float, int],  # pylint: disable=invalid-name
        w: Union[float, int]  # pylint: disable=invalid-name
    ) -> None:
        """Quaternion contructor.

        Args:
            x (Union[float, int]):
                Quaternion x component
            y (Union[float, int]):
                Quaternion y component
            z (Union[float, int]):
                Quaternion z component
            w (Union[float, int]):
                Quaternion w component

        """
        raise NotImplementedError

    def __init__(self, *args, **kwargs):
        """Implement common quaternion contructor.

        Args:
            *args (Any):
                Arbitrary positional arguments.
            **kwargs (Any):
                Arbitrary named arguments.

        """
        args += tuple(kwargs.values())
        if isinstance(args[0], float) or isinstance(args[0], int):
            super().__init__([*args], normalize=True)
        else:
            args = [*args]
            args[1] = True  # normalize
            super().__init__(*args)

    def conjugate(self) -> 'Quaternion':
        """Return conjugated quaternion.

        Returns:
            Quaternion:
                Conjugated quaternion (x, y, z, w)

        >>> q = Quaternion.from_euler('xyz', [45, -90, 180], degrees=True)
        >>> q.conjugate().as_euler('xyz', degrees=True)
        [ 90. -45.  90.]

        .. _Source:
            https://www.meccanismocomplesso.org/en/hamiltons-quaternions-and-3d-rotation-with-python

        """
        return self.__class__(
            x=-self.x,
            y=-self.y,
            z=-self.z,
            w=self.w
        )

    def __str__(self) -> str:
        """Return string representation of the object.

        Returns:
            str:
                Representation of the object.

        """
        return ", ".join(
            (
                str(self.x),
                str(self.y),
                str(self.z),
                str(self.w)
            )
        )
