#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# SPDX-License-Identifier: AGPL-3.0-or-later

"""```vector`` module of ``math`` package."""

from typing import Union
from .typing import Reproducible


class CoordinateVector(Reproducible):
    """Cartesian coordinate vector."""

    _x: float = float(0.0)
    _y: float = float(0.0)
    _z: float = float(0.0)

    @property
    def x(self) -> float:  # pylint: disable=invalid-name
        """float: x coordinate."""
        return self._x

    @property
    def y(self) -> float:  # pylint: disable=invalid-name
        """float: y coordinate."""
        return self._y

    @property
    def z(self) -> float:  # pylint: disable=invalid-name
        """float: z coordinate."""
        return self._z

    def __init__(
        self,
        x: Union[float, int],  # pylint: disable=invalid-name
        y: Union[float, int],  # pylint: disable=invalid-name
        z: Union[float, int]  # pylint: disable=invalid-name
    ) -> None:
        """Coordinate vector contructor.

        Args:
            x (Union[float, int]):
                Cartesian x coordinate
            y (Union[float, int]):
                Cartesian y coordinate
            z (Union[float, int]):
                Cartesian z coordinate

        """
        self._x = float(x)
        self._y = float(y)
        self._z = float(z)

    @classmethod
    def identity(cls) -> 'CoordinateVector':
        """Create an identity position and returns it.

        Returns:
            Position:
                Created identity position (x, y, z, w)

        >>> CoordinateVector.identity()
        CoordinateVector(0.0, 0.0, 0.0)

        """
        return cls(
            x=cls._x,
            y=cls._x,
            z=cls._x
        )

    def __str__(self) -> str:
        """Return string representation of the object.

        Returns:
            str:
                Representation of the object.

        """
        return ", ".join(
            (
                str(self.x),
                str(self.y),
                str(self.z)
            )
        )
