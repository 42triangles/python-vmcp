#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# SPDX-License-Identifier: AGPL-3.0-or-later

"""``typing`` module of ``math`` package."""

from abc import ABCMeta


class Reproducible(metaclass=ABCMeta):
    """Reproducible instance.

    >>> eval(repr(Reproducible()))
        Reproducible()

    """

    def __repr__(self) -> str:
        """Return a string, representing the object in a reconstructable way.

        Returns:
            str:
                Representing the object in a reconstructable way.

        """
        cls = self.__class__
        module = f"{cls.__module__}." if cls.__module__ != "__main__" else ''
        return f"{module}{cls.__qualname__}({str(self)})"
