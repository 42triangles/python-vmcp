#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# SPDX-License-Identifier: AGPL-3.0-or-later

"""``events`` module of ``vmcp`` package."""

from typing import (
    Tuple,
    Union,
    overload
)
from .osc.typing import (
    ARG_READERNAME,
    ARG_DATAUNPACK,
    ARG_ADDRESS
)
from .typing import (
    Position,
    Rotation,
    Scale,
    Bone,
    DeviceType,
    ModelState,
    CalibrationState,
    CalibrationMode,
    TrackingState
)
from .math import (
    CoordinateVector,
    Quaternion
)
from .math.typing import (
    Reproducible
)


class Event:
    """Abstract event class."""

    ARGUMENTS: Tuple[str] = ARG_READERNAME + ARG_DATAUNPACK
    """Tuple[str]: OSC argument scheme."""

    ADDRESS: str
    """str: OSC address."""

    @property
    def channel_name(self) -> str:
        """str: Channel name."""
        return str(self._channel_name)

    def __init__(
        self,
        channel_name: str
    ) -> None:
        """Event constructor.

        Args:
            channel_name (str):
                Channel name.

        Raises:
            NotImplementedError:
                If abstract `Event` is instantiated.

        """
        self._channel_name = channel_name
        # pylint: disable=unidiomatic-typecheck
        if type(self) is Event:
            raise NotImplementedError

    def __str__(self) -> str:
        """Return string representation of the object.

        Returns:
            str:
                Representation of the object.

        """
        return str(self._channel_name)


class TransformEvent(Event):
    """Abstract transform event class."""

    @property
    def position(self) -> Position:
        """Position: 3D position."""
        return CoordinateVector(
            self._position_x,
            self._position_y,
            self._position_z
        )

    @property
    def rotation(self) -> Rotation:
        """Rotation: 3D rotation."""
        return Quaternion(
            self._rotation_x,
            self._rotation_y,
            self._rotation_z,
            self._rotation_w
        )

    def __init__(
        self,
        channel_name: str,
        joint_name: str,
        position_x: float,
        position_y: float,
        position_z: float,
        rotation_x: float,
        rotation_y: float,
        rotation_z: float,
        rotation_w: float
    ) -> None:
        """Event constructor.

        Args:
            channel_name (str):
                Channel name.
            joint_name (str):
                Joint name.
            position_x (float):
                Cartesian x coordinate.
            position_y (float):
                Cartesian y coordinate.
            position_z (float):
                Cartesian z coordinate.
            rotation_x (float):
                Quaternion x component.
            rotation_y (float):
                Quaternion y component.
            rotation_z (float):
                Quaternion z component.
            rotation_w (float):
                Quaternion w component.

        Raises:
            NotImplementedError:
                If abstract `TransformEvent` is instantiated.

        """
        # Joint
        self._joint_name = joint_name
        # Position
        self._position_x = position_x
        self._position_y = position_y
        self._position_z = position_z
        # Rotation
        self._rotation_x = rotation_x
        self._rotation_y = rotation_y
        self._rotation_z = rotation_z
        self._rotation_w = rotation_w
        # pylint: disable=unidiomatic-typecheck
        if type(self) is TransformEvent:
            raise NotImplementedError
        super().__init__(
            channel_name
        )

    def __str__(self) -> str:
        """Return string representation of the object.

        Returns:
            str:
                Representation of the object.

        """
        return ", ".join(
            (
                super().__str__(),
                str(self._joint_name),
                str(self._position_x),
                str(self._position_y),
                str(self._position_z),
                str(self._rotation_x),
                str(self._rotation_y),
                str(self._rotation_z),
                str(self._rotation_w)
            )
        )


class RootTransformEvent(TransformEvent, Reproducible):
    """Root transform event."""

    ADDRESS: str = "/VMC/Ext/Root/Pos"
    """str: OSC address."""

    @property
    def scale(self) -> Scale:
        """Scale: Additional scaling (division)."""
        return Scale(
            self._scale_width,
            self._scale_heigth,
            self._scale_length
        )

    @property
    def offset(self) -> Position:
        """Position: Additional (negative) offset."""
        return CoordinateVector(
            self._offset_x,
            self._offset_y,
            self._offset_z
        )

    @overload
    def __init__(
        self,
        channel_name: str,
        joint_name: str,
        position_x: float,
        position_y: float,
        position_z: float,
        rotation_x: float,
        rotation_y: float,
        rotation_z: float,
        rotation_w: float
    ) -> None:
        """Event constructor.

        Since VMC protocol specification v2.0.0.

        Args:
            channel_name (str):
                Channel name.
            joint_name (str):
                Joint name.
            position_x (float):
                Cartesian x coordinate.
            position_y (float):
                Cartesian y coordinate.
            position_z (float):
                Cartesian z coordinate.
            rotation_x (float):
                Quaternion x component.
            rotation_y (float):
                Quaternion y component.
            rotation_z (float):
                Quaternion z component.
            rotation_w (float):
                Quaternion w component.

        """
        raise NotImplementedError

    @overload
    def __init__(
        self,
        channel_name: str,
        joint_name: str,
        position_x: float,
        position_y: float,
        position_z: float,
        rotation_x: float,
        rotation_y: float,
        rotation_z: float,
        rotation_w: float,
        scale_width: float,
        scale_heigth: float,
        scale_length: float,
        offset_x: float,
        offset_y: float,
        offset_z: float
    ) -> None:
        """Event constructor.

        Since VMC protocol specification v2.1.0.

        Args:
            channel_name (str):
                Channel name.
            joint_name (str):
                Joint name.
            position_x (float):
                Cartesian x coordinate.
            position_y (float):
                Cartesian y coordinate.
            position_z (float):
                Cartesian z coordinate.
            rotation_x (float):
                Quaternion x component.
            rotation_y (float):
                Quaternion y component.
            rotation_z (float):
                Quaternion z component.
            rotation_w (float):
                Quaternion w component.
            scale_width (float):
                Scaling width dimension.
            scale_heigth (float):
                Scaling heigth dimension.
            scale_length (float):
                Scaling length dimension.
            offset_x (float):
                Additional (negative) cartesian x coordinate offset.
            offset_y (float):
                Additional (negative) cartesian y coordinate offset.
            offset_z (float):
                Additional (negative) cartesian z coordinate offset.

        """
        raise NotImplementedError

    def __init__(
        self,
        channel_name: str,
        joint_name: str,
        position_x: float,
        position_y: float,
        position_z: float,
        rotation_x: float,
        rotation_y: float,
        rotation_z: float,
        rotation_w: float,
        scale_width: float = 1.0,
        scale_heigth: float = 1.0,
        scale_length: float = 1.0,
        offset_x: float = 0.0,
        offset_y: float = 0.0,
        offset_z: float = 0.0
    ) -> None:
        """Implement event constructor.

        Args:
            channel_name (str):
                Channel name.
            joint_name (str):
                Joint name.
            position_x (float):
                Cartesian x coordinate.
            position_y (float):
                Cartesian y coordinate.
            position_z (float):
                Cartesian z coordinate.
            rotation_x (float):
                Quaternion x component.
            rotation_y (float):
                Quaternion y component.
            rotation_z (float):
                Quaternion z component.
            rotation_w (float):
                Quaternion w component.
            scale_width (Optional[float]):
                Scaling width dimension.
            scale_heigth (Optional[float]):
                Scaling heigth dimension.
            scale_length (Optional[float]):
                Scaling length dimension.
            offset_x (Optional[float]):
                Additional (negative) cartesian x coordinate offset.
            offset_y (Optional[float]):
                Additional (negative) cartesian y coordinate offset.
            offset_z (Optional[float]):
                Additional (negative) cartesian z coordinate offset.

        """
        super().__init__(
            channel_name,
            joint_name,
            position_x,
            position_y,
            position_z,
            rotation_x,
            rotation_y,
            rotation_z,
            rotation_w
        )
        # Since VMC protocol specification v2.1.0.
        # Scaling
        self._scale_width = scale_width
        self._scale_heigth = scale_heigth
        self._scale_length = scale_length
        # Offset
        self._offset_x = offset_x
        self._offset_y = offset_y
        self._offset_z = offset_z

    def __str__(self) -> str:
        """Return string representation of the object.

        Returns:
            str:
                Representation of the object.

        """
        return ", ".join(
            (
                super().__str__(),
                str(self._scale_width),
                str(self._scale_heigth),
                str(self._scale_length),
                str(self._offset_x),
                str(self._offset_y),
                str(self._offset_z)
            )
        )


class BoneTransformEvent(TransformEvent, Reproducible):
    """Bone transform event."""

    ADDRESS: str = "/VMC/Ext/Bone/Pos"
    """str: OSC address."""

    @property
    def bone(self) -> Bone:
        """Bone: Joint bone."""
        return Bone(self._joint_name)


class BlendShapeEvent(Event, Reproducible):
    """Blend shape event."""

    ADDRESS: str = "/VMC/Ext/Blend/Val"
    """str: OSC address."""

    @property
    def key(self) -> str:
        """str: Blend shape key."""
        return str(self._key)

    @property
    def value(self) -> float:
        """float: Blend shape value."""
        return float(self._value)

    def __init__(
        self,
        channel_name: str,
        key: str,
        value: float
    ) -> None:
        """Event constructor.

        Args:
            channel_name (str):
                Channel name.
            key (str):
                Blend shape key.
            value (float):
                Blend shape value.

        """
        super().__init__(
            channel_name
        )
        self._key = key
        self._value = value

    def __str__(self) -> str:
        """Return string representation of the object.

        Returns:
            str:
                Representation of the object.

        """
        return ", ".join(
            (
                super().__str__(),
                str(self._key),
                str(self._value)
            )
        )


class BlendShapeApplyEvent(Event, Reproducible):
    """Blend shape apply event."""

    ADDRESS: str = "/VMC/Ext/Blend/Apply"
    """str: OSC address."""


class DeviceTransformEvent(TransformEvent, Reproducible):
    """Device transform receiving event."""

    DType = DeviceType

    ARGUMENTS: Tuple[str] = ARG_READERNAME + ARG_DATAUNPACK + ARG_ADDRESS
    """Tuple[str]: OSC argument scheme."""

    ADDRESS: str = f"/VMC/Ext/{{{','.join([t.value for t in DType])}}}//"
    """str: OSC address."""

    @property
    def device_type(self) -> DeviceType:
        """DeviceType: Device type."""
        return DeviceType(self._address.split('/')[3])

    @property
    def device_label(self) -> str:
        """str: OpenVR device label or serial."""
        return str(self._joint_name)

    @property
    def is_local(self) -> bool:
        """bool: If transform is relative to avatar, word space otherwise."""
        return self._address.count('/') > 4

    def __init__(
        self,
        channel_name: str,
        joint_name: str,
        position_x: float,
        position_y: float,
        position_z: float,
        rotation_x: float,
        rotation_y: float,
        rotation_z: float,
        rotation_w: float,
        address: str
    ) -> None:
        """Event constructor.

        Args:
            channel_name (str):
                Channel name.
            joint_name (str):
                Joint name.
            position_x (float):
                Cartesian x coordinate.
            position_y (float):
                Cartesian y coordinate.
            position_z (float):
                Cartesian z coordinate.
            rotation_x (float):
                Quaternion x component.
            rotation_y (float):
                Quaternion y component.
            rotation_z (float):
                Quaternion z component.
            rotation_w (float):
                Quaternion w component.
            address (str):
                OSC address.

        """
        super().__init__(
            channel_name,
            joint_name,
            position_x,
            position_y,
            position_z,
            rotation_x,
            rotation_y,
            rotation_z,
            rotation_w
        )
        self._address = address

    def __str__(self) -> str:
        """Return string representation of the object.

        Returns:
            str:
                Representation of the object.

        """
        return ", ".join(
            (
                super().__str__(),
                str(self._address)
            )
        )


class StateEvent(Event, Reproducible):
    """Availability state event."""

    ADDRESS: str = "/VMC/Ext/OK"
    """str: OSC address."""

    @property
    def model_state(self) -> ModelState:
        """ModelState: Model state."""
        return ModelState(self._model_state)

    @property
    def calibration_state(self) -> Union[CalibrationState, None]:
        """Union[CalibrationState, None]: Calibration state."""
        if self._calibration_state is not None:
            return CalibrationState(self._calibration_state)
        else:
            return None

    @property
    def calibration_mode(self) -> Union[CalibrationMode, None]:
        """Union[CalibrationMode, None]: Calibration mode."""
        if self._calibration_mode is not None:
            return CalibrationMode(self._calibration_mode)
        else:
            return None

    @property
    def tracking_state(self) -> Union[TrackingState, None]:
        """Union[TrackingState, None]: Tracking state."""
        if self._tracking_state is not None:
            return TrackingState(self._tracking_state)
        else:
            return None

    @overload
    def __init__(
        self,
        channel_name: str,
        model_state: int
    ) -> None:
        """Event constructor.

        Args:
            channel_name (str):
                Channel name.
            model_state (int):
                Model state.

        """
        raise NotImplementedError

    @overload
    def __init__(
        self,
        channel_name: str,
        model_state: int,
        calibration_state: int,
        calibration_mode: int
    ) -> None:
        """Event constructor.

        Since VMC protocol specification v2.5.0.

        Args:
            channel_name (str):
                Channel name.
            model_state (int):
                Model state.
            calibration_state (int):
                Calibration state.
            calibration_mode (int):
                Calibration mode.

        """
        raise NotImplementedError

    @overload
    def __init__(
        self,
        channel_name: str,
        model_state: int,
        calibration_state: int,
        calibration_mode: int,
        tracking_state: int
    ) -> None:
        """Event constructor.

        Since VMC protocol specification v2.7.0.

        Args:
            channel_name (str):
                Channel name.
            model_state (int):
                Model state.
            calibration_state (int):
                Calibration state.
            calibration_mode (int):
                Calibration mode.
            tracking_state (int):
                Tracking state.

        """
        raise NotImplementedError

    def __init__(
        self,
        channel_name: str,
        model_state: int,
        calibration_state: int = None,
        calibration_mode: int = None,
        tracking_state: int = None
    ) -> None:
        """Implement event constructor.

        Args:
            channel_name (str):
                Channel name.
            model_state (int):
                Model state.
            calibration_state (Optional[int]):
                Calibration state.
            calibration_mode (Optional[int]):
                Calibration mode.
            tracking_state (Optional[int]):
                Tracking state.

        """
        super().__init__(
            channel_name
        )
        self._model_state = model_state
        self._calibration_state = calibration_state
        self._calibration_mode = calibration_mode
        self._tracking_state = tracking_state

    def __str__(self) -> str:
        """Return string representation of the object.

        Returns:
            str:
                Representation of the object.

        """
        return ", ".join(
            (
                super().__str__(),
                str(self._model_state),
                str(self._calibration_state),
                str(self._calibration_mode),
                str(self._tracking_state)
            )
        )


class RelativeTimeEvent(Event, Reproducible):
    """Relative time event."""

    ADDRESS: str = "/VMC/Ext/T"
    """str: OSC address."""

    @property
    def delta(self) -> float:
        """float: Relative frame time."""
        return float(self._delta)

    def __init__(
        self,
        channel_name: str,
        delta: float
    ) -> None:
        """Event constructor.

        Args:
            channel_name (str):
                Channel name.
            delta (float):
                Relative frame time.

        """
        super().__init__(
            channel_name
        )
        self._delta = delta

    def __str__(self) -> str:
        """Return string representation of the object.

        Returns:
            str:
                Representation of the object.

        """
        return ", ".join(
            (
                super().__str__(),
                str(self._delta),
            )
        )
