#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# SPDX-License-Identifier: AGPL-3.0-or-later

"""Protocol layer module of ``vmcp`` package."""

from typing import (
    Type,
    Callable,
    Tuple,
    overload
)
from .osc.typing import Message
from .typing import (
    Bone,
    Position,
    Rotation,
    Scale,
    DeviceType,
    BlendShapeKey,
    ModelState,
    CalibrationState,
    CalibrationMode,
    TrackingState
)
from .events import Event


class VMCP:
    """Virtual Motion Capture (VMC) protocol layer for OSC.

    .. _Specification:
    https://protocol.vmc.info/english

    """

    @classmethod
    def on_receive(
        cls,
        event: Type[Event],
        handler: Callable[[Event], None]
    ) -> Tuple[str, Callable[..., None], Tuple[str]]:
        """Event-based receiving handler registration helper method.

        Args:
            event (Type[Event]):
                Event type class.
            handler (Callable[[Event], None]):
                Arbitrary handler function or method.

        Returns:
            tuple: Arguments for usage with ``osc.Receiver.register_handler()``
                str:
                    Channel name.
                Callable[..., None]:
                    Lambda expression to pass occurring events to the handler.
                Tuple[str]:
                    OSC argument scheme.

        """
        return (
            event.ADDRESS,
            lambda *args: handler(event(*args)),
            event.ARGUMENTS
        )

    @overload
    @classmethod
    def root_transform(
        cls,
        position: Position,
        rotation: Rotation
    ) -> Message:
        """Root transform.

        Since VMC protocol specification v2.0.0.

        Args:
            position (Position):
                Transform position.
            rotation (Rotation):
                Transform rotation.

        Returns:
            Message:
                OSC command.

        """
        raise NotImplementedError

    @overload
    @classmethod
    def root_transform(
        cls,
        position: Position,
        rotation: Rotation,
        scale: Scale,
        offset: Position,
    ) -> Message:
        """Root transform.

        Since VMC protocol specification v2.1.0.

        Args:
            position (Position):
                Transform position.
            rotation (Rotation):
                Transform rotation.
            scale (Size):
                Additional scaling (division).
            offset (Position):
                Additional (negative) offset.

        Returns:
            Message:
                OSC command.

        """
        raise NotImplementedError

    @classmethod
    def root_transform(
        cls,
        position: Position,
        rotation: Rotation,
        scale: Scale = None,
        offset: Position = None,
    ) -> Message:
        """Implement root transform.

        Since VMC protocol specification v2.0.0.
        ``scale`` and ``offset`` support since v2.1.0.

        Args:
            position (Position):
                Transform position.
            rotation (Rotation):
                Transform rotation.
            scale (Optional[Size]):
                Additional scaling (division).
            offset (Optional[Position]):
                Additional (negative) offset.

        Returns:
            Message:
                OSC command.

        """
        if scale is not None and offset is not None:
            # since VMC protocol specification v2.1.0
            return Message(
                "/VMC/Ext/Root/Pos",
                ",sfffffffffffff",
                (
                    "root",
                    position.x,
                    position.y,
                    position.z,
                    rotation.x,
                    rotation.y,
                    rotation.z,
                    rotation.w,
                    scale.width,
                    scale.height,
                    scale.length,
                    offset.x,
                    offset.y,
                    offset.z
                )
            )
        else:
            # since VMC protocol specification v2.0.0
            return Message(
                "/VMC/Ext/Root/Pos",
                ",sfffffff",
                (
                    "root",
                    position.x,
                    position.y,
                    position.z,
                    rotation.x,
                    rotation.y,
                    rotation.z,
                    rotation.w,
                )
            )

    @classmethod
    def bone_transform(
        cls,
        bone: Bone,
        position: Position,
        rotation: Rotation
    ) -> Message:
        """Bone transform.

        Args:
            bone (Bone):
                Bone.
            position (Position):
                Transform position.
            rotation (Rotation):
                Transform rotation.

        Returns:
            Message:
                OSC command.

        """
        return Message(
            "/VMC/Ext/Bone/Pos",
            ",sfffffff",
            (
                bone.value,
                position.x,
                position.y,
                position.z,
                rotation.x,
                rotation.y,
                rotation.z,
                rotation.w
            )
        )

    @classmethod
    def device_transform(
        cls,
        device_type: DeviceType,
        device_label: str,
        position: Position,
        rotation: Rotation,
        local: bool = False
    ) -> Message:
        """Device transform.

        Since VMC protocol specification v2.2.0.
        ``local`` transform support since v2.3.0.

        Args:
            device_type (DeviceType):
                Device type.
            device_label (str):
                OpenVR device label or serial.
            position (Position):
                Transform position.
            rotation (Rotation):
                Transform rotation.
            local (bool):
                If ``True`` the transform is relative to the avatar,
                word space otherwise (Default).

        Returns:
            Message:
                OSC command.

        """
        if local:
            address = f"/VMC/Ext/{device_type.value}/Pos/Local"
        else:
            address = f"/VMC/Ext/{device_type.value}/Pos"
        return Message(
            address,
            ",sfffffff",
            (
                device_label,
                position.x,
                position.y,
                position.z,
                rotation.x,
                rotation.y,
                rotation.z,
                rotation.w
            )
        )

    @classmethod
    def blendshape(
        cls,
        key: BlendShapeKey,
        value: float
    ) -> Message:
        """Blendshape.

        Requires `blendshape_apply()` afterwards to take effect.

        Args:
            key (BlendShapeKey):
                Blend shape key.
            value (float):
                Blend shape value.

        Returns:
            Message:
                OSC command.

        """
        return Message(
            "/VMC/Ext/Blend/Val",
            ",sf",
            (
                key.value,
                value
            )
        )

    @classmethod
    def blendshape_apply(
        cls,
    ) -> Message:
        """Apply blendshape(s).

        Returns:
            Message:
                OSC command.

        """
        return Message(
            "/VMC/Ext/Blend/Apply",
            None,
            tuple()
        )

    @overload
    @classmethod
    def state(
        cls,
        model_state: ModelState
    ) -> Message:
        """Availability state.

        Args:
            model_state (ModelState):
                Model state.

        Returns:
            Message:
                OSC command.

        """
        raise NotImplementedError

    @overload
    @classmethod
    def state(
        cls,
        model_state: ModelState,
        calibration_state: CalibrationState,
        calibration_mode: CalibrationMode
    ) -> Message:
        """Availability state.

        Since VMC protocol specification v2.5.0.

        Args:
            model_state (ModelState):
                Model state.
            calibration_state (CalibrationState):
                Calibration state.
            calibration_mode (CalibrationMode):
                Calibration mode.

        Returns:
            Message:
                OSC command.

        """
        raise NotImplementedError

    @overload
    @classmethod
    def state(
        cls,
        model_state: ModelState,
        calibration_state: CalibrationState,
        calibration_mode: CalibrationMode,
        tracking_state: TrackingState
    ) -> Message:
        """Availability state.

        Since VMC protocol specification v2.7.0.

        Args:
            model_state (ModelState):
                Model state.
            calibration_state (CalibrationState):
                Calibration state.
            calibration_mode (CalibrationMode):
                Calibration mode.
            tracking_state (TrackingState):
                Trackling state.

        Returns:
            Message:
                OSC command.

        """
        raise NotImplementedError

    @classmethod
    def state(
        cls,
        model_state: ModelState,
        calibration_state: CalibrationState = None,
        calibration_mode: CalibrationMode = None,
        tracking_state: TrackingState = None
    ) -> Message:
        """Implement availability state.

        Args:
            model_state (ModelState):
                Model state.
            calibration_state (Optional[CalibrationState]):
                Calibration state.
            calibration_mode (Optional[CalibrationMode]):
                Calibration mode.
            tracking_state (Optional[TrackingState]):
                Trackling state.

        Returns:
            Message:
                OSC command.

        """
        if (
            calibration_state is not None and
            calibration_mode is not None
        ):
            if tracking_state is not None:
                # since VMC protocol specification v2.7.0
                return Message(
                    "/VMC/Ext/OK",
                    ",iiii",
                    (
                        model_state,
                        calibration_state,
                        calibration_mode,
                        tracking_state
                    )
                )
            else:
                # since VMC protocol specification v2.5.0
                return Message(
                    "/VMC/Ext/OK",
                    ",iii",
                    (
                        model_state,
                        calibration_state,
                        calibration_mode
                    )
                )
        else:
            return Message(
                "/VMC/Ext/OK",
                ",i",
                (
                    model_state
                )
            )

    @classmethod
    def time(
        cls,
        delta: float
    ) -> Message:
        """Relative frame time.

        Args:
            delta (float):
                Relative time.

        Returns:
            Message:
                OSC command.
        """
        return Message(
            "/VMC/Ext/T",
            ",f",
            (
                delta
            )
        )
