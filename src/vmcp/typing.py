#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# SPDX-License-Identifier: AGPL-3.0-or-later

"""``typing`` module of ```vmcp``` package."""

from enum import Enum
from abc import (
    ABCMeta,
    abstractmethod
)
from typing import (
    Protocol,
    runtime_checkable,
    Dict,
    overload
)
from .osc.typing import (
    Sender,
    Receiver
)
from .math.typing import Reproducible


# Availability
class ModelState(Enum):
    """VMC model state."""

    NOT_LOADED = 0
    LOADED = 1


class CalibrationState(Enum):
    """VMC calibration state."""

    UNCALIBRATED = 0
    WAITING_FOR_CALIBRATING = 1
    CALIBRATING = 2
    CALIBRATED = 3


class CalibrationMode(Enum):
    """VMC calibration mode."""

    NORMAL = 0,
    MR_HAND = 1
    MR_FLOOR = 2


class TrackingState(Enum):
    """VMC tracking state."""

    BAD = 0
    OK = 1


# Devices
class DeviceType(Enum):
    """VMC device types."""

    HMD = "Hmd"
    CONTROLLER = "Con"
    TRACKER = "Tra"


# 3D Space
class Bone(Enum):
    """All 55 HumanBodyBones from Unity."""

    HIPS = "Hips"
    LEFT_UPPER_LEG = "LeftUpperLeg"
    RIGHT_UPPER_LEG = "RightUpperLeg"
    LEFT_LOWER_LEG = "LeftLowerLeg"
    RIGHT_LOWER_LEG = "RightLowerLeg"
    LEFT_FOOT = "LeftFoot"
    RIGHT_FOOT = "RightFoot"
    SPINE = "Spine"
    CHEST = "Chest"
    UPPER_CHEST = "UpperChest"
    NECK = "Neck"
    HEAD = "Head"
    LEFT_SHOULDER = "LeftShoulder"
    RIGHT_SHOULDER = "RightShoulder"
    LEFT_UPPER_ARM = "LeftUpperArm"
    RIGHT_UPPER_ARM = "RightUpperArm"
    LEFT_LOWER_ARM = "LeftLowerArm"
    RIGHT_LOWER_ARM = "RightLowerArm"
    LEFT_HAND = "LeftHand"
    RIGHT_HAND = "RightHand"
    LEFT_TOES = "LeftToes"
    RIGHT_TOES = "RightToes"
    LEFT_EYE = "LeftEye"
    RIGHT_EYE = "RightEye"
    JAW = "Jaw"
    LEFT_THUMB_PROXIMAL = "LeftThumbProximal"
    LEFT_THUMB_INTERMEDIATE = "LeftThumbIntermediate"
    LEFT_THUMB_DISTAL = "LeftThumbDistal"
    LEFT_INDEX_PROXIMAL = "LeftIndexProximal"
    LEFT_INDEX_INTERMEDIATE = "LeftIndexIntermediate"
    LEFT_INDEX_DISTAL = "LeftIndexDistal"
    LEFT_MIDDLE_PROXIMAL = "LeftMiddleProximal"
    LEFT_MIDDLE_INTERMEDIATE = "LeftMiddleIntermediate"
    LEFT_MIDDLE_DISTAL = "LeftMiddleDistal"
    LEFT_RING_PROXIMAL = "LeftRingProximal"
    LEFT_RING_INTERMEDIATE = "LeftRingIntermediate"
    LEFT_RING_DISTAL = "LeftRingDistal"
    LEFT_LITTLE_PROXIMAL = "LeftLittleProximal"
    LEFT_LITTLE_INTERMEDIATE = "LeftLittleIntermediate"
    LEFT_LITTLE_DISTAL = "LeftLittleDistal"
    RIGHT_THUMB_PROXIMAL = "RightThumbProximal"
    RIGHT_THUMB_INTERMEDIATE = "RightThumbIntermediate"
    RIGHT_THUMB_DISTAL = "RightThumbDistal"
    RIGHT_INDEX_PROXIMAL = "RightIndexProximal"
    RIGHT_INDEX_INTERMEDIATE = "RightIndexIntermediate"
    RIGHT_INDEX_DISTAL = "RightIndexDistal"
    RIGHT_MIDDLE_PROXIMAL = "RightMiddleProximal"
    RIGHT_MIDDLE_INTERMEDIATE = "RightMiddleIntermediate"
    RIGHT_MIDDLE_DISTAL = "RightMiddleDistal"
    RIGHT_RING_PROXIMAL = "RightRingProximal"
    RIGHT_RING_INTERMEDIATE = "RightRingIntermediate"
    RIGHT_RING_DISTAL = "RightRingDistal"
    RIGHT_LITTLE_PROXIMAL = "RightLittleProximal"
    RIGHT_LITTLE_INTERMEDIATE = "RightLittleIntermediate"
    RIGHT_LITTLE_DISTAL = "RightLittleDistal"


class BlendShapeKey(Enum):
    """BlendShapeKey interface."""


@runtime_checkable
class Position(Protocol, metaclass=ABCMeta):
    """Position interface."""

    @property
    @abstractmethod
    def x(self) -> float:  # pylint: disable=invalid-name
        """float: Cartesian x coordinate."""
        raise NotImplementedError

    @property
    @abstractmethod
    def y(self) -> float:  # pylint: disable=invalid-name
        """float: Cartesian y coordinate."""
        raise NotImplementedError

    @property
    @abstractmethod
    def z(self) -> float:  # pylint: disable=invalid-name
        """float: Cartesian z coordinate."""
        raise NotImplementedError


@runtime_checkable
class Rotation(Protocol, metaclass=ABCMeta):
    """Rotation interface."""

    @property
    @abstractmethod
    def x(self) -> float:  # pylint: disable=invalid-name
        """float: Quaternion x component."""
        raise NotImplementedError

    @property
    @abstractmethod
    def y(self) -> float:  # pylint: disable=invalid-name
        """float: Quaternion y component."""
        raise NotImplementedError

    @property
    @abstractmethod
    def z(self) -> float:  # pylint: disable=invalid-name
        """float: Quaternion z component."""
        raise NotImplementedError

    @property
    @abstractmethod
    def w(self) -> float:  # pylint: disable=invalid-name
        """float: Quaternion w component."""
        raise NotImplementedError


class Scale(Reproducible):
    """Scale."""

    @property
    def width(self) -> float:
        """float: Width dimension."""
        return float(self._width)

    @property
    def height(self) -> float:
        """float: Height dimension."""
        return float(self._height)

    @property
    def length(self) -> float:
        """float: Length dimension."""
        return float(self._length)

    @overload
    def __init__(
        self,
        uniformly: float
    ) -> None:
        """Scale constructor.

        Args:
            uniformly (float):
                All dimensions.

        """
        raise NotImplementedError

    @overload
    def __init__(
        self,
        width: float,
        height: float,
        length: float
    ) -> None:
        """Scale constructor.

        Args:
            width (float):
                Width dimension.
            height (float):
                Height dimension.
            length (float):
                Length dimension.

        """
        raise NotImplementedError

    def __init__(
        self,
        *args: float,
        **kwargs: float
    ) -> None:
        """Implement scale constructor.

        Args:
            *args (float):
                1 OR 3 dimension arguments.
        Raises:
            ValueError:
                If invalid arguments are given.

        """
        args += tuple(kwargs.values())
        match len(args):
            case 1:
                self._width = self._height = self._length = args[0]
            case 3:
                self._width = args[0]
                self._height = args[1]
                self._length = args[2]
            case _:
                raise ValueError(f"Invalid parameters given: {str(args)}")

    def __str__(self) -> str:
        """Return string representation of the object.

        Returns:
            str:
                Representation of the object.

        """
        return ", ".join(
            (
                str(self._width),
                str(self._height),
                str(self._length)
            )
        )


# Communication
@runtime_checkable
class Time(Protocol, metaclass=ABCMeta):
    """Time interface."""

    @abstractmethod
    def delta(self, offset: float = 0.0, precision: int = 6) -> float:
        """Return time delta in seconds with it's fractions as float.

        Args:
            offset (float):
                Delta offset (Default: 0.0).
            precision (int):
                Round the results to ``precision`` digits
                after the decimal point (Default: 6).

        Returns:
            float:
                Time delta in seconds with it's fractions.

        >>> Time().delta()
        0.0

        """
        raise NotImplementedError


@runtime_checkable
class Assistant(Protocol, metaclass=ABCMeta):
    """VMC protocol assistant interface."""

    @property
    @abstractmethod
    def _sender(self) -> Dict[str, Sender]:
        raise NotImplementedError


@runtime_checkable
class Performer(Protocol, metaclass=ABCMeta):
    """VMC protocol performer interface."""

    @property
    @abstractmethod
    def _sender(self) -> Dict[str, Sender]:
        raise NotImplementedError

    @property
    @abstractmethod
    def _receiver(self) -> Dict[str, Receiver]:
        raise NotImplementedError


@runtime_checkable
class Marionette(Protocol, metaclass=ABCMeta):
    """VMC protocol marionette interface."""

    @property
    @abstractmethod
    def _receiver(self) -> Dict[str, Receiver]:
        raise NotImplementedError
